# storage-service

## General information
This microservice stores all of the interactions, AST Information, and eye coordinates that is sent by the Pygmy Client. 
The data is stored in a file as JSON Strings after each other in the order that they came into the service.

The `FileSorter.java` class is responsible for sorting the log-files where the data is stored. Currently, one unsorted version is saved and a sorted version, to make sure no information is lost. 

The `TimeConverter.java` class is responsible for converting the timestamps to a normalized version. See 'Information regarding the timestamps' for some deeper information and possible improvements.

### Information regarding the timestamps
Since the eye-tracker is using a different kind of timestamp that cannot be received from the web page (for example the time when a button was pressed), they cannot be compared to each other. We have thought about ways of fixing this which resulted in the solution we have today. In this version, we get the first entries of the two types of timestamps, `msSinceEpoch`(generated by us developers) and `ts`(generated by the eye tracker), and use them to get the relative timestamps compared to them. 

It is not a perfect solution, and this might need to be optimized further. 

## Run
To run the PuR service, run:

On windows: ```gradle run```
On Mac:  ```./gradlew run```

To format the code: 

On windows ```gradle goJF```
On Mac: ```./gradlew goJF```

To run and format the code:

On windows: ```gradle cleanrun```
On Mac: ```./gradlew cleanrun```

## Test the service in the terminal

To get test the ping-pong service: `curl -X GET localhost:8006/ping`

To store eye tracking data: `curl -d '[data]' -X  POST localhost:8006/storeCoordinates`

Example: `curl -d '[{"ts":932158487242,"x":0.06323587894439697,"y":0.5029458403587341}]' -X  POST localhost:8006/storeCoordinates`

Example for storing the tracked elements: `curl -d '[{ "ts": "000", "element" : "button", "element-id" : "submit-button", "coordinates" : {"x": "0", "y" : "12"}}, {"ts": "001", "element" : "div", "element-id" : "content-area", "coordinates" : {"x": "400", "y" : "100"}}]' -X POST localhost:8001/storeElements`



