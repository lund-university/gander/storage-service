package storage.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/** Sorting class for our JSON Files using merge sort. */
public class FileSorter {

  /**
   * Copies the content of a file to another.
   *
   * @param filePath Path to the file that should be copied.
   * @return New filepath.
   */
  public static String copyFile(String filePath) {
    // Find logs map
    String path = createLogsPath();
    // Find file with filePath
    File originalFile = findFile(filePath);
    // Load file content to a list of JSONObjects
    JSONObject[] list = loadListFromFile(originalFile);
    // Perform mergesort
    mergeSort(list, 0, list.length - 1);
    // Create new file
    File newFile = findFile(filePath + "-sorted");
    // Add sorted list to new file
    printListToFile(newFile, list);
    // Return
    return newFile.getAbsolutePath();
  }

  /**
   * Takes a list and prints it to a specific file.
   *
   * @param file The file that the list should be printed to.
   * @param list The list that should be printed.
   */
  private static void printListToFile(File file, JSONObject[] list) {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
      writer.write("[ \n");
      int idxOfLast = list.length;
      int idxStart = 0;
      for (JSONObject jsonObject : list) {
        writer.write(jsonObject.toJSONString());
        if (idxStart < idxOfLast) {
          writer.write(", \n");
        }
        idxStart++;
      }
      writer.write("]");
      writer.close();
    } catch (IOException e) {
      // TODO: Catch error
      e.printStackTrace();
    }
  }

  /**
   * Merge sort algorithm.
   *
   * @param list The list of JSONObjects we want to sort.
   * @param start The start index.
   * @param end The end index.
   */
  private static void mergeSort(JSONObject[] list, int start, int end) {
    if (start < end) {
      int mid = (start + (end - 1)) / 2;
      mergeSort(list, start, mid);
      mergeSort(list, mid + 1, end);

      merge(list, start, mid, end);
    }
  }

  /**
   * Merge algorithm.
   *
   * @param list The list of JSONObjects we want to sort.
   * @param start The start index.
   * @param mid The middle index.
   * @param end The end index.
   */
  private static void merge(JSONObject[] list, int start, int mid, int end) {
    int size1 = mid - start + 1, size2 = end - mid;

    JSONObject left[] = new JSONObject[size1], right[] = new JSONObject[size2];

    for (int i = 0; i < size1; i++) {
      left[i] = list[start + i];
    }
    for (int j = 0; j < size2; j++) {
      right[j] = list[mid + 1 + j];
    }

    int i = 0, j = 0;

    int k = start;
    while (i < size1 && j < size2) {
      if (getTs(left[i]) <= getTs(right[j])) {
        list[k] = left[i];
        i++;
      } else {
        list[k] = right[j];
        j++;
      }
      k++;
    }

    while (i < size1) {
      list[k] = left[i];
      i++;
      k++;
    }

    while (j < size2) {
      list[k] = right[j];
      j++;
      k++;
    }
  }

  /**
   * Retrieves the timestamp of the entry.
   *
   * @param jsonObject The object that contains the entry.
   * @return The timestamp in a long format, 0 if no timestamp was found.
   */
  private static long getTs(JSONObject jsonObject) {
    if (jsonObject.get("ts") != null) {
      return Long.parseLong(jsonObject.get("ts").toString());
    } else if (jsonObject.get("msSinceEpoch") != null) {
      return Long.parseLong(jsonObject.get("msSinceEpoch").toString());
    }
    return 0;
  }

  /**
   * Tries to create a file if it was not already found.
   *
   * @param filePath The path to the requested file.
   * @return The file if it was found or created.
   * @throws IOException
   */
  private static File findFile(String filePath) {
    try {
      File file = new File(filePath);
      if (!file.exists()) {
        System.out.println(filePath);
        file.createNewFile();
      }
      return file;
    } catch (IOException e) {
      // TODO: Catch error
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Finds the path to the logs where we store all of our actions that is sent from the client.
   *
   * @throws IOException
   */
  private static String createLogsPath() {
    String logsFolderPath = Paths.get("logs").toAbsolutePath().toString();
    try {
      return Files.createDirectories(Paths.get(logsFolderPath)).toFile().getAbsolutePath();
    } catch (IOException e) {
      // TODO: Catch error
    }
    return null;
  }

  /**
   * Takes a file and retrieves the list of JSON Objects that it contains.
   *
   * @param originalFile The file we want to extract the JSON Objects from.
   * @return The list of JSON Objects found in the file.
   * @throws IOException
   * @throws org.json.simple.parser.ParseException
   */
  private static JSONObject[] loadListFromFile(File originalFile) {
    try {
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(new FileReader(originalFile));
      JSONArray jsonArray = (JSONArray) obj;
      JSONObject objList[] = new JSONObject[jsonArray.size()];
      for (int i = 0; i < jsonArray.size(); i++) {
        objList[i] = (JSONObject) jsonArray.get(i);
      }
      return (JSONObject[]) objList;
    } catch (IOException e) {
      // TODO: Catch error
      e.printStackTrace();
    } catch (org.json.simple.parser.ParseException e) {
      // TODO: Catch error
      e.printStackTrace();
    }
    return null;
  }
}
