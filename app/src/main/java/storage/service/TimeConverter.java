package storage.service;

/** The Pygmy Time Converter */
public class TimeConverter {
  public long ts_0;
  public long msSinceEpoch_0;

  /**
   * The constructor for the Time Converter class.
   *
   * @param ts_0 The value of ts of our first entry.
   * @param msSinceEpoch_0 The value of msSinceEpoch of our first entry.
   */
  public TimeConverter(long ts_0, long msSinceEpoch_0) {
    this.ts_0 = ts_0;
    this.msSinceEpoch_0 = msSinceEpoch_0;
  }

  /**
   * Normalize the ts by subtracting our initial ts-value.
   *
   * @param ts_1 The ts we want to normalize.
   * @return The normalized ts.
   */
  public long getConvertedTs(long ts_1) {
    return ts_1 - ts_0;
  }

  /**
   * Normalize the msSinceEpoch by subtracting our initial msSinceEpoch-value.
   *
   * @param msSinceEpoch_1 The msSinceEpoch we want to normalize.
   * @return The normalized msSinceEpoch.
   */
  public long getConvertedMsSinceEpoch(long msSinceEpoch_1) {
    return msSinceEpoch_1 - msSinceEpoch_0;
  }
}
