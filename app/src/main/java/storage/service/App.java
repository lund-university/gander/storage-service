package storage.service;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/** Pygmy Storage Service */
public class App {
  private static final int DEFAULT_PORT = 8006;
  private static String logsFolderPath;

  public static void main(String[] args) {
    int port = args.length > 0 ? parsePortArgument(args[0]) : DEFAULT_PORT;
    port(port);
    Logger logger = setupLogger(port);
    logger.info("-- STARTING STORAGE SERVICE ON PORT " + port + " --");
    StorageService storageService = new StorageService(logger);

    get("/ping", (req, res) -> "pong");
    post("/storeCoordinates", (req, res) -> storageService.postEyeTrackingData(req, res));
    post("/storeElements", (req, res) -> storageService.postElements(req, res));
    post("/storeInteraction", (req, res) -> storageService.postInteraction(req, res));
    post("/initSynchronization", (req, res) -> storageService.initSynchronization(req, res));
    post("/createNewFile", (req, res) -> storageService.createNewFile(req, res));
    post("/storeBufferData", (req,res) -> storageService.storeBufferData(req, res));
  }

  /**
   * Parses the incoming argument that should be containing the port number.
   *
   * @param portNbr the port number as a String
   * @return the port number as an int
   */
  public static int parsePortArgument(String portNbr) {
    try {
      return Integer.parseInt(portNbr);
    } catch (NumberFormatException e) {
      System.out.println(
          "Could not resolve the argument to a port number, using the default port number "
              + DEFAULT_PORT);
      return DEFAULT_PORT;
    }
  }

  /**
   * Initializes a Logger object. Creates a new entry in the log-folder.
   *
   * @param port port number the service is active on
   * @return the Logger object
   */
  public static Logger setupLogger(int port) {
    createLogFolder();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
    Logger logger = Logger.getLogger(App.class.getName());
    FileHandler fh;
    try {
      fh =
          new FileHandler(
              logsFolderPath + "/Log_" + dtf.format(LocalDateTime.now()) + "_port" + port + ".log",
              true);
      fh.setFormatter(new SimpleFormatter());
      logger.addHandler(fh);
      // Change the line below for more/less logging info
      logger.setLevel(Level.ALL);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return logger;
  }

  /** Creates a folder for the logs, if it doesn't already exist. */
  private static void createLogFolder() {
    logsFolderPath = Paths.get("debugLogs").toAbsolutePath().toString();
    try {
      Files.createDirectories(Paths.get(logsFolderPath));
    } catch (IOException e) {
      System.out.println("Error while creating the logs directory. ");
    }
  }
}
