package storage.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import spark.Request;
import spark.Response;

/** Pygmy Storage Service */
class StorageService {
  private String logsFolderPath;
  private String currentFilePath;
  private String bufferLogFolderPath;
  private String currentBufferLogFilePath;
  private Logger logger;
  private TimeConverter timeConverter;

  private static final String CLASS_NAME = StorageService.class.getName();

  /**
   * Storage Service constructor.
   *
   * @param log The logger used for debugging.
   */
  public StorageService(Logger log) {
    this.logger = log;
    logger.info("Storage Service started");
    createLogFolder();
    clearStorage();

  }

  /**
   * Creates the logs folder used to store our actions that is sent from the client.
   *
   * @throws IOException
   */
  private void createLogFolder() {
    logger.entering(CLASS_NAME, "createLogFolder");
    logsFolderPath = Paths.get("logs").toAbsolutePath().toString();
    //Create Folder for the bufferdata logs
    bufferLogFolderPath = Paths.get("bufferLogs").toAbsolutePath().toString();
    try {
      Files.createDirectories(Paths.get(logsFolderPath));

      Files.createDirectories(Paths.get(bufferLogFolderPath));

      logger.exiting(CLASS_NAME, "createLogFolder");
      logger.exiting(CLASS_NAME, "createBufferLogFolder");
    } catch (IOException e) {
      logger.severe("Error while creating the logs directory. ");
    }
  }

  /**
   * Clears the storage of the action logs, i.e. creating a new log and setting the currentFilePath
   * to the new file.
   *
   * @throws Exception
   */
  private void clearStorage() {
    logger.entering(CLASS_NAME, "clearStorage");
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    LocalDateTime now = LocalDateTime.now();
    String currDtf = dtf.format(now);
    try {
      currentFilePath = Paths.get(logsFolderPath, currDtf).toString();
      File logFile = new File(currentFilePath);
      logFile.createNewFile();

      currentBufferLogFilePath = Paths.get(bufferLogFolderPath, currDtf).toString();
      File bufferLogFile = new File(currentBufferLogFilePath);
      bufferLogFile.createNewFile();
      

      logger.exiting(CLASS_NAME, "clearStorage");
    } catch (Exception e) {
      logger.severe(String.format("Trouble with clearing storage file: %s", e.getMessage()));
      System.out.println("Trouble with clearing storage file");
      System.out.println(e.getMessage());
    }
  }
/*

  private void sortJsonFile(String currentFilePath) {
    try {
      BufferedReader file = new BufferedReader(new FileReader(currentFilePath));
      StringBuffer inputBuffer = new StringBuffer();
      String line;

      while ((line = file.readLine()) != null) {
        line += ",";
        inputBuffer.append(line);
        inputBuffer.append("\n");
      }
      // Removing the new line and the last ',' to make a json array
      inputBuffer.deleteCharAt(inputBuffer.length() - 1);
      inputBuffer.deleteCharAt(inputBuffer.length() - 1);
      inputBuffer.append("]");
      file.close();
      FileOutputStream fileOut = new FileOutputStream(currentFilePath);
      fileOut.write(inputBuffer.toString().getBytes());
      fileOut.close();

    } catch (IOException e) {
      logger.severe("Error with sorting json file: " + e.getMessage());
    }
  }
 */


  /**
   * Posts the received data to the current file.
   *
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status as a JSON String.
   */
  public String postEyeTrackingData(Request req, Response res) {
    logger.entering(CLASS_NAME, "postEyeTrackingData");
    
    String reqStr = req.body();
    JSONArray dataArray  = getJsonArray(reqStr);
    
    JSONObject jsonResponse = new JSONObject();

    try {

      Path storagePath = FileSystems.getDefault().getPath(currentFilePath).toAbsolutePath();
      File file = new File(storagePath.toString());
      BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
      //System.out.println("Data: " + dataArray);
      Iterator<JSONObject> iterator = dataArray.iterator();

      while (iterator.hasNext()) {
        JSONObject data = iterator.next();


        data = dataToCorrectTimeFormat(data);
        //System.out.println(data);
        if (data != null) {
          appendToLogFile(data, writer);
        }
      }

      writer.close();
      res.status(HttpURLConnection.HTTP_CREATED);
      jsonResponse.put("status", "SUCCESS");
    } catch (Exception e) {

      jsonResponse.put("status", "ERROR");

      logger.severe("error: " + e.getMessage());
      return jsonResponse.toString();
    }
    logger.exiting(CLASS_NAME, "postEyeTrackingData", jsonResponse);
    return jsonResponse.toString();
  }


  /**
   * Posts the received buffer data to the current buffer log file.
   *
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status as a JSON String.
   */
  public String storeBufferData(Request req, Response res) {
    logger.entering(CLASS_NAME, "storeBufferData");

    JSONArray dataArray  = getJsonArray(req.body());
    JSONObject jsonResponse = new JSONObject();
    
    Path storagePath = FileSystems.getDefault().getPath(currentBufferLogFilePath).toAbsolutePath();
    File file = new File(storagePath.toString());
    
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {

      Iterator<JSONObject> iterator = dataArray.iterator();
      JSONArray tempArr = new JSONArray();
      while (iterator.hasNext()) {
        JSONObject data = iterator.next();

        if (data.get("ts").toString().startsWith("-")) {
          continue;
        } 
        tempArr.add((JSONObject) dataToCorrectTimeFormat(data));
        /*if (data != null) {
          writer.append(data.toString());
          writer.append(",\n");
        }*/
      }
      writer.append(tempArr.toJSONString());

      writer.close();
      res.status(HttpURLConnection.HTTP_CREATED);
      jsonResponse.put("status", "SUCCESS");
    
    } catch (Exception e) {

      jsonResponse.put("status", "ERROR");
      logger.severe("error: " + e.getMessage());
      
      return jsonResponse.toString();
    }

    logger.exiting(CLASS_NAME, "postEyeTrackingData", jsonResponse);
    return jsonResponse.toString();
  }

  /**
   * Posts the action that shows the element that the client has found that the person is looking
   * at.
   *
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status as a JSON String.
   */
  public String postElements(Request req, Response res) {
    logger.entering(CLASS_NAME, "postElements");
    JSONArray dataArray = getJsonArray(req.body());
    JSONObject jsonResponse = new JSONObject();

    try {

      Path storagePath = FileSystems.getDefault().getPath(currentFilePath).toAbsolutePath();
      File file = new File(storagePath.toString());
      BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));

      Iterator<JSONObject> iterator = dataArray.iterator();
      while (iterator.hasNext()) {

        JSONObject data = iterator.next();
        data = dataToCorrectTimeFormat(data);
        if (data != null) {
          appendToLogFile(data, writer);
        }
      }

      writer.close();
      res.status(HttpURLConnection.HTTP_CREATED);
      jsonResponse.put("status", "SUCCESS");
    } catch (Exception e) {

      jsonResponse.put("status", "ERROR");

      logger.severe("error: " + e.getMessage());
      return jsonResponse.toString();
    }
    logger.exiting(CLASS_NAME, "postElements", jsonResponse.toString());
    return jsonResponse.toString();
  }

  /**
   * Posts the action that shows the element that the client has found that the person has been
   * interacting with, and what kind of interaction. This includes button presses, etc.
   *
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status as a JSON String.
   */
  public String postInteraction(Request req, Response res) {
    JSONObject data = getJsonObject(req.body());
    JSONObject jsonResponse = new JSONObject();

    try {

      Path storagePath = FileSystems.getDefault().getPath(currentFilePath).toAbsolutePath();
      File file = new File(storagePath.toString());
      BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
      data = dataToCorrectTimeFormat(data);
      logger.entering(CLASS_NAME, "post interaction");
      if (data != null) {
        appendToLogFile(data, writer);
      }

      writer.close();
      res.status(HttpURLConnection.HTTP_CREATED);
      jsonResponse.put("status", "SUCCESS");
    } catch (Exception e) {

      jsonResponse.put("status", "ERROR");
      logger.severe("error: "+ e.getMessage() + " this is the req: " + req.body());
      System.out.println("error: " + e.getMessage());
      
      return jsonResponse.toString();
    }

    return jsonResponse.toString();
  }

  /**
   * Appends the provided JSON object to the log file using the specified writer.
   * This method is synchronized to ensure thread-safe access to the file.
   *
   * @param data   The JSON object to append to the log file.
   * @param writer The BufferedWriter used to write to the log file.
   * @throws IOException if an I/O error occurs while writing to the file.
   */
  private synchronized void appendToLogFile(JSONObject data, BufferedWriter writer) throws IOException {
    writer.append(data.toString());
    writer.append("\n");
  }

  /**
   * Gets a JSON Array from a string.
   *
   * @param body The string where we want to retrieve the JSON Array from.
   * @return The JSON Array
   * @throws org.json.simple.parser.ParseException
   */
  private JSONArray getJsonArray(String body)  {
    logger.entering(CLASS_NAME, "getJsonArray", body);
    JSONParser parser = new JSONParser();
    JSONArray json = new JSONArray();
    try {
      json = (JSONArray) parser.parse(body);
      logger.exiting(CLASS_NAME, "getJsonArray", json.toString());
      return json;
    } catch (org.json.simple.parser.ParseException e) {
      logger.severe("error: " + e.getMessage() + "\t"+body);
      return null;
    }
  }

  /**
   * Gets a JSON Object from a string.
   *
   * @param body The string where we want to retrieve the JSON Object from.
   * @return The JSON Object
   */
  private JSONObject getJsonObject(String body) {

    JSONParser parser = new JSONParser();
    JSONObject json = new JSONObject();
    try {
      return json = (JSONObject) parser.parse(body);

    } catch (Exception e) {
      logger.entering(CLASS_NAME, "getJsonObject");
      logger.severe("error: "+e.getMessage() + " Object: "+body);
      return null;
    }
  }

  /**
   * Method used to streamline the two different timestamps, ts and msSinceEpoch. Since the
   * eye-tracker are using different types of timestamps that cannot be converted to a ISO standard
   * and depends on computer settings, we have to use this way of streamlining them.
   *
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status as a JSON String.
   */
  public String initSynchronization(Request req, Response res) {
    logger.entering(CLASS_NAME, "initSynchronization");
    JSONObject data = getJsonObject(req.body());
    JSONObject jsonResponse = new JSONObject();

    try {
      String ts = data.get("ts").toString();
      String msSinceEpoch = data.get("msSinceEpoch").toString();
      timeConverter = new TimeConverter(Long.parseLong(ts), Long.parseLong(msSinceEpoch));
      res.status(HttpURLConnection.HTTP_CREATED);
      jsonResponse.put("status", "SUCCESS");
    } catch (Exception e) {

      jsonResponse.put("status", "ERROR");
      jsonResponse.put("message", e.getMessage());
      logger.severe(e.getMessage());
      logger.exiting(CLASS_NAME, "initSynchronization");
      return jsonResponse.toString();
    }
    logger.exiting(CLASS_NAME, "initSynchronization", jsonResponse.toString());
    return jsonResponse.toString();
  }

  /**
   * Changes the time format to our normalized timestamp, to be able to sort the actions depending
   * on their timestamp.
   *
   * @param obj The JSON Object containing the timestamp.
   * @return The JSON Object with the normalized timestamp.
   */
  private JSONObject dataToCorrectTimeFormat(JSONObject obj) {
    boolean msTries=false;
    logger.entering(CLASS_NAME, "dataToCorrectTimeFormat", obj.toString());

    if (obj.get("msSinceEpoch") != null) {
      try{
        long msSinceEpoch = Long.parseLong(obj.get("msSinceEpoch").toString());
        obj.put("msSinceEpoch", epochToNormalized(msSinceEpoch));
      }
      catch(Exception e){
        msTries=true;  
      }
      if(msTries){
        try{
          JSONObject timeStamp = (JSONObject) obj.get("msSinceEpoch");
          long msSinceEpoch1 = Long.parseLong(timeStamp.get("TimeReq").toString());
          long msSinceEpoch2 = Long.parseLong(timeStamp.get("TimeRes").toString());
          timeStamp.put("TimeReq", epochToNormalized(msSinceEpoch1));
          timeStamp.put("TimeRes", epochToNormalized(msSinceEpoch2));
          obj.put("msSinceEpoch", timeStamp);
        } 
        catch(Exception e){
          logger.severe(e.getMessage() + " couldn't normalize msSinceEpoch");
        }
      }
    } 
    /** remove this since we now want to keep ts to synch with old logs
    else if (obj.get("ts") != null) {
      long ts = Long.parseLong(obj.get("ts").toString());
      obj.put("ts", tsToNormalized(ts));
    } 
    */
    else {
      logger.exiting(CLASS_NAME, "dataToCorrectTimeFormat", null);
      return null; 
    }
    logger.exiting(CLASS_NAME, "dataToCorrectTimeFormat", obj);
    return obj;
  }

  /**
   * Normalizes the ts-timestamps by using our time converter.
   *
   * @param ts The timestamp in a long format.
   * @return A string with the normalized ts
   */
  private String tsToNormalized(long ts) {
    logger.entering(CLASS_NAME, "tsToNormalized");
    if (timeConverter == null) {
      logger.exiting(CLASS_NAME, "tsToNormalized", null);
      return null;
    }
    logger.exiting(CLASS_NAME, "tsToNormalized");
    return String.valueOf(timeConverter.getConvertedTs(ts));
  }

  /**
   * Normalizes the msSinceEpoch-timestamps by using our time converter.
   *
   * @param ms The timestamp in a long format.
   * @return A string with the normalized msSinceEpoch
   */
  private String epochToNormalized(long ms) {
    logger.entering(CLASS_NAME, "epochToNormalized");
    if (timeConverter == null) {
      logger.exiting(CLASS_NAME, "epochToNormalized", null);
      return String.valueOf(ms);
    }
    logger.exiting(CLASS_NAME, "epochToNormalized");
    return String.valueOf(timeConverter.getConvertedMsSinceEpoch(ms));
  }

  /**
   * Creates a new file for us to put our next log entries in.
   *
   * @param req The HTTP Request.
   * @param res The HTTP Response.
   * @return The status as a JSON Object.
   */
  public JSONObject createNewFile(Request req, Response res) {
    createLogFolder();
    clearStorage();
    JSONObject jsonResponse = new JSONObject();
    jsonResponse.put("Status", "OK");
    res.status(HttpURLConnection.HTTP_OK);
    return jsonResponse;
  }


}
